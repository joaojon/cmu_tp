package pt.ipp.estg.cmu_tp;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

public class HomeFragment extends Fragment {
    public Application application;
    private final ProfileFragment profileFragment = new ProfileFragment();
    private final DashboardFragment dashboardFragment = new DashboardFragment();
    private final ScannerFragment scannerFragment = new ScannerFragment();
    private final VenuesFragment venuesFragment = new VenuesFragment();
    private final ChoseQuizFragment choseQuizFragment = new ChoseQuizFragment();

    public HomeFragment() {
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        BottomNavigationView bottomNavigation = view.findViewById(R.id.bot_nav);
        bottomNavigation.setOnNavigationItemSelectedListener(navListener);

        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction().replace(R.id.container, new ProfileFragment()).commit();
        }
        return view;
    }

    private final BottomNavigationView.OnNavigationItemSelectedListener navListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment selectedFragment = null;

            switch (item.getItemId()) {
                case R.id.profile:
                    selectedFragment = profileFragment;
                    break;
                case R.id.dashboard:
                    selectedFragment = dashboardFragment;
                    dashboardFragment.setApp(application);
                    break;
                case R.id.qrcode:
                    selectedFragment = scannerFragment;
                    scannerFragment.setApplication(application);
                    break;
                case R.id.stadium:
                    selectedFragment = venuesFragment;
                    break;
                case R.id.quiz:
                    selectedFragment = choseQuizFragment;
                    choseQuizFragment.setApplication(application);
                    break;
            }

            getFragmentManager().beginTransaction().replace(R.id.container, selectedFragment).commit();
            return true;
        }
    };
}