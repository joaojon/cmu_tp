package pt.ipp.estg.cmu_tp;

import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;


import java.io.IOException;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {
    private SupportMapFragment mMapFragment;
    private GoogleMap mGoogleMap;
    private String address, venueName;
    private double lat = 0, lon = 0;


    public MapFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        address = getArguments().getString("address");
        venueName = getArguments().getString("venueName");

        List<Address> addresses;
        Geocoder coder = new Geocoder(getContext());
        try {
            addresses = coder.getFromLocationName(address + "," + venueName, 5);
            Address location = addresses.get(0);
            lat = location.getLatitude();
            lon = location.getLongitude();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_map, container, false);

        mMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        LatLng city = new LatLng(lat, lon);
        mGoogleMap.addMarker(new MarkerOptions().position(city).title(venueName));

        CircleOptions circleOptions = new CircleOptions();
        circleOptions.center(city);
        circleOptions.radius(300);
        circleOptions.strokeColor(Color.parseColor("#1d001f"));
        circleOptions.fillColor(0x220000FF);
        circleOptions.strokeWidth(2);
        mGoogleMap.addCircle(circleOptions);

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(city));
    }
}
