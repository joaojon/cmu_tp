package pt.ipp.estg.cmu_tp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pt.ipp.estg.cmu_tp.Adapters.TeamSpinnerAdapter;
import pt.ipp.estg.cmu_tp.ExternAPI.APIClient;
import pt.ipp.estg.cmu_tp.ExternAPI.FootbalAPITeamResponse;
import pt.ipp.estg.cmu_tp.ExternAPI.FootballAPI;
import pt.ipp.estg.cmu_tp.ExternAPI.Response;
import pt.ipp.estg.cmu_tp.ExternAPI.Team;
import retrofit2.Call;
import retrofit2.Callback;

public class RegisterFragment extends Fragment implements View.OnClickListener {
    protected FootbalAPITeamResponse footbalAPITeamResponseList;
    private TextInputEditText email, password, confirmPassword, name, phoneNumber;
    private Spinner teamSpinner;
    private TeamSpinnerAdapter teamAdapter;
    private Team selectedTeam;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    // Access a Cloud Firestore instance from your Activity
    private FirebaseFirestore db;
    private Button cancelBtn, registerBtn;

    public RegisterFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        email = view.findViewById(R.id.email);

        name = view.findViewById(R.id.name);
        phoneNumber = view.findViewById(R.id.phoneNumber);
        password = view.findViewById(R.id.password);
        confirmPassword = view.findViewById(R.id.confirmPassword);
        cancelBtn = view.findViewById(R.id.btnCancel);
        registerBtn = view.findViewById(R.id.btnRegister);

        cancelBtn.setOnClickListener(this);
        registerBtn.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
        mUser = FirebaseAuth.getInstance().getCurrentUser();
        db = FirebaseFirestore.getInstance();

        footbalAPITeamResponseList = new FootbalAPITeamResponse();

        FootballAPI apiClient = APIClient.getClient().create(FootballAPI.class);
        Call<FootbalAPITeamResponse> call = apiClient.getTeam("39", "2020");

        teamSpinner = view.findViewById(R.id.teamSpinner);

        call.enqueue(new Callback<FootbalAPITeamResponse>() {
            @Override
            public void onResponse(Call<FootbalAPITeamResponse> call, retrofit2.Response<FootbalAPITeamResponse> response) {
                footbalAPITeamResponseList = response.body();
                List<Response> responses = footbalAPITeamResponseList.getResponse();

                teamAdapter = new TeamSpinnerAdapter(getContext(), responses);

                teamSpinner.setAdapter(teamAdapter);

                teamSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        selectedTeam = footbalAPITeamResponseList.getResponse().get(position).getTeam();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        Toast.makeText(getContext(), "Please, choose your team!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void onFailure(Call<FootbalAPITeamResponse> call, Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
            }
        });

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCancel:
                LoginFragment loginFragment = new LoginFragment();
                FragmentTransaction tr = getFragmentManager().beginTransaction();
                tr.replace(R.id.login_fragment, loginFragment);
                tr.addToBackStack(null);
                tr.commit();
                break;
            case R.id.btnRegister:
                register(
                        email.getText().toString(),
                        password.getText().toString(),
                        selectedTeam.getId(),
                        selectedTeam.getName()
                );
                break;
        }
    }

    private void register(final String email, String password, final int teamId, final String teamName) {
        if (!checkForm()) {
            Toast.makeText(getContext(), "Some field is missing or password don't match!", Toast.LENGTH_SHORT).show();

            return;
        }

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(requireActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Map<String, Object> user = new HashMap<>();
                            user.put("name", name.getText().toString());
                            user.put("email", email);
                            user.put("phoneNumber", phoneNumber.getText().toString());
                            user.put("points", 0);
                            user.put("teamId", teamId);
                            user.put("teamName", teamName);

                            Calendar cal = Calendar.getInstance();
                            cal.setTime(new Date());
                            cal.add(Calendar.MINUTE, -30);
                            Date dateBack = cal.getTime();

                            user.put("lastDateOfAnswered", new Timestamp(dateBack));

                            db.collection("users").add(user)
                                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                                        @Override
                                        public void onSuccess(DocumentReference documentReference) {
                                            Toast.makeText(getContext(), "Register with success! To continue please login in your account.", Toast.LENGTH_SHORT).show();

                                            LoginFragment loginFragment = new LoginFragment();
                                            FragmentTransaction tr = getParentFragmentManager().beginTransaction();
                                            tr.replace(R.id.login_fragment, loginFragment);
                                            tr.addToBackStack(null);
                                            tr.commit();
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(getContext(), "Register failed, please validate all fields!", Toast.LENGTH_SHORT).show();
                                        }
                                    });
                        } else {
                            Toast.makeText(getContext(), "Register failed, please validate all fields!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private boolean checkForm() {
        return !email.getText().toString().isEmpty() && !password.getText().toString().isEmpty() && !confirmPassword.getText().toString().isEmpty() && confirmPassword.getText().toString().equals(password.getText().toString()) && !name.getText().toString().isEmpty() && !phoneNumber.getText().toString().isEmpty();
    }
}