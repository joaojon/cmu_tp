package pt.ipp.estg.cmu_tp.Tabs;

import android.app.Application;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.image.charts.ImageCharts;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import pt.ipp.estg.cmu_tp.R;
import pt.ipp.estg.cmu_tp.Repo.DatabaseRepository;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class ChartsTabFragment extends Fragment {

    private ArrayList<Integer> listByType, listByResult;
    private CardView chartCard, percentChartCard;
    private TextView chartError, percentChartError;
    private ImageView chart, percentChart;

    private Application app;
    private DatabaseRepository dbRoom;
    private String mUser;

    public ChartsTabFragment() {
        // Required empty public constructor
    }

    public void setAppAndEmail(Application app, String email) {
        this.app = app;
        mUser = email;

        dbRoom = new DatabaseRepository(app);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_charts_tab, container, false);

        chartError = view.findViewById(R.id.chartError);
        chart = view.findViewById(R.id.chart);
        percentChartError = view.findViewById(R.id.percentageChartError);
        percentChart = view.findViewById(R.id.percentageChart);
        chartCard = view.findViewById(R.id.chartCard);
        percentChartCard = view.findViewById(R.id.percentageChartCard);

        try {
            getChart();
        } catch (ExecutionException | UnsupportedEncodingException | InterruptedException | MalformedURLException | NoSuchAlgorithmException | InvalidKeyException e) {
            updateLayoutNoChart(chartError, chart, chartCard);
        }

        try {
            getPercentageChart();
        } catch (ExecutionException | InterruptedException | MalformedURLException | NoSuchAlgorithmException | InvalidKeyException | UnsupportedEncodingException e) {
            updateLayoutNoChart(percentChartError, percentChart, percentChartCard);
        }

        return view;
    }

    private void updateLayout(ImageView chartToShow, String url) {
        Picasso.with(getContext())
                .load(url)
                .into(chartToShow);
    }

    private void updateLayoutNoChart(TextView chartErrorToShow, ImageView chartToHide, CardView cardTohide) {
        chartErrorToShow.setVisibility(View.VISIBLE);
        chartToHide.setVisibility(View.GONE);
        cardTohide.setVisibility(View.GONE);
    }

    private void getPercentageChart() throws ExecutionException, InterruptedException, MalformedURLException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        listByResult = dbRoom.countPercentage(mUser).get();

        if (listByResult.size() != 0) {

            String labels = "";
            String values = "t:";

            if (listByResult.get(0) > 0) {
                labels += "Correct (" + listByResult.get(0) + ")";

                values += listByResult.get(0);
            }

            if (listByResult.get(1) > 0) {
                labels += "Incorrect (" + listByResult.get(1) + ")";

                if (values.length() > 2) {
                    values += ",";
                }

                values += listByResult.get(1);
            }

            ImageCharts pie = new ImageCharts()
                    .cht("p")
                    .chco("F39C12,00FF85,009688")
                    .chl(labels)
                    .chlps("font.size,36")
                    .chd(values)
                    .chs("700x700");

            if (listByType.get(0) == 0 && listByType.get(1) == 0) {
                updateLayoutNoChart(percentChartError, percentChart, percentChartCard);
            } else {
                updateLayout(percentChart, pie.toURL());
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void getChart() throws ExecutionException, InterruptedException, MalformedURLException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {

        listByType = dbRoom.countHistoryByType(mUser).get();

        if (listByType.size() != 0) {

            String labels = "";

            if (listByType.get(0) > 0) {
                labels += "Venue (" + listByType.get(0) + ")";
            }

            if (listByType.get(1) > 0) {

                if (labels.length() > 0) {
                    labels += "|";
                }

                labels += "Player (" + listByType.get(1) + ")";
            }

            if (listByType.get(2) > 0) {
                if (labels.length() > 0) {
                    String.join("|");
                }

                labels += "Team (" + listByType.get(2) + ")";
            }

            String values = "t:";

            if (listByType.get(0) > 0) {
                values += listByType.get(0);
            }

            if (listByType.get(1) > 0) {

                if (values.length() > 2) {
                    values += ",";
                }

                values += listByType.get(1);
            }

            if (listByType.get(2) > 0) {

                if (values.length() > 2) {
                    values += ",";
                }

                values += listByType.get(2);
            }

            ImageCharts pie = new ImageCharts()
                    .cht("p")
                    .chco("F39C12,00FF85,009688")
                    .chl(labels)
                    .chlps("font.size,36")
                    .chd(values)
                    .chs("700x700");

            if (listByType.get(0) == 0 && listByType.get(1) == 0 && listByType.get(2) == 0) {
                updateLayoutNoChart(chartError, chart, chartCard);
            } else {
                updateLayout(chart, pie.toURL());
            }
        }
    }
}