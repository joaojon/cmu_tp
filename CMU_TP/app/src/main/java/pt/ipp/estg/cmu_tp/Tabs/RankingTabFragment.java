package pt.ipp.estg.cmu_tp.Tabs;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;

import pt.ipp.estg.cmu_tp.Adapters.UsersAdapter;
import pt.ipp.estg.cmu_tp.R;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class RankingTabFragment extends Fragment {
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();

    private UsersAdapter adapter;
    private RecyclerView rvUsers;
    private LinearLayoutManager layoutManager;

    public RankingTabFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ranking_tab, container, false);
        rvUsers = view.findViewById(R.id.rvUsers);

        this.db.collection("users")
                .orderBy("points", Query.Direction.DESCENDING)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        List<DocumentSnapshot> docs = task.getResult().getDocuments();

                        adapter = new UsersAdapter(docs, getContext());
                        layoutManager = new LinearLayoutManager(getContext());
                        rvUsers.setLayoutManager(layoutManager);
                        rvUsers.setAdapter(adapter);
                    }
                });

        return view;
    }
}