package pt.ipp.estg.cmu_tp;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

/**
 * A simple {@link Fragment} subclass.
 */
public class QuizFragment extends Fragment implements View.OnClickListener {
    private Button venueBtn, teamBtn, playerBtn;
    private int type = 0;

    private Application application;

    public QuizFragment() {
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quiz, container, false);

        venueBtn = view.findViewById(R.id.btnVenue);
        teamBtn = view.findViewById(R.id.btnTeam);
        playerBtn = view.findViewById(R.id.btnPlayer);

        playerBtn.setOnClickListener(this);
        venueBtn.setOnClickListener(this);
        teamBtn.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnVenue:
                type = 1;
                break;
            case R.id.btnPlayer:
                type = 2;
                break;
            case R.id.btnTeam:
                type = 3;
                break;
        }
        if (type != 0) {
            QuestionFragment questionFragment = new QuestionFragment();
            questionFragment.setTrial(true);
            questionFragment.setAppAndType(application, type);
            FragmentTransaction tr = getFragmentManager().beginTransaction();
            tr.replace(R.id.container, questionFragment);
            tr.addToBackStack(null);
            tr.commit();
        }
    }
}