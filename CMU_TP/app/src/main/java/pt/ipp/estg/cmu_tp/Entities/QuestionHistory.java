package pt.ipp.estg.cmu_tp.Entities;

import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class QuestionHistory {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public int id = 0;

    @ColumnInfo(name = "user_email")
    public String userEmail;

    @ColumnInfo(name = "question")
    public String question;

    @ColumnInfo(name = "question_type")
    public int questionType;

    @ColumnInfo(name = "points")
    public int points;

    @ColumnInfo(name = "correct_answer")
    public String correctAnswer;

    @ColumnInfo(name = "user_answer")
    public String userAnswer;

    @ColumnInfo(name = "timestamp")
    public String timestamp;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public QuestionHistory(String userEmail, String question, int questionType, int points, String correctAnswer, String userAnswer) {
        this.userEmail = userEmail;
        this.question = question;
        this.questionType = questionType;
        this.points = points;
        this.correctAnswer = correctAnswer;
        this.userAnswer = userAnswer;
        timestamp = java.time.LocalDateTime.now().toString();
    }
}
