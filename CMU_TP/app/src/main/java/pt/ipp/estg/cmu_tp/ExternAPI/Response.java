package pt.ipp.estg.cmu_tp.ExternAPI;

public class Response {
    public Team team;
    public Venue venue;

    public Response(Team team, Venue venue) {
        this.team = team;
        this.venue = venue;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public Venue getVenue() {
        return venue;
    }
}
