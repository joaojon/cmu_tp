package pt.ipp.estg.cmu_tp;

import android.app.Application;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ChoseQuizFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ChoseQuizFragment extends Fragment implements View.OnClickListener {
    private Button btnPoints, btnTrial;
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private final Random rand = new Random();
    static int type;
    private FirebaseUser mUser;
    private Map<String, Object> userLogged;
    private com.google.firebase.Timestamp lastAnsweredQuestion;
    private TextView countDown, textCountDown;
    private CountDownTimer mCountDownTimer;
    Date afterAddingThirtyMins, currentDate;

    private Application application;


    public ChoseQuizFragment() {
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chose_quiz, container, false);

        btnPoints = view.findViewById(R.id.btnPoints);
        btnTrial = view.findViewById(R.id.btnTrial);
        countDown = view.findViewById(R.id.text_view_countdown);
        textCountDown = view.findViewById(R.id.textOfCountdown);

        btnPoints.setOnClickListener(this);
        btnTrial.setOnClickListener(this);

        mUser = FirebaseAuth.getInstance().getCurrentUser();

        db.collection("users")
                .whereEqualTo("email", mUser.getEmail())
                .limit(1)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            QuerySnapshot doc = task.getResult();
                            userLogged = doc.getDocuments().get(0).getData();

                            lastAnsweredQuestion = (com.google.firebase.Timestamp) userLogged.get("lastDateOfAnswered");
                            Date date = lastAnsweredQuestion.toDate();
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);

                            long t = cal.getTimeInMillis();

                            afterAddingThirtyMins = new Date(t + (30 * 60000));
                            currentDate = new Date();

                            if (currentDate.before(afterAddingThirtyMins)) {
                                countDown.setVisibility(View.VISIBLE);
                                textCountDown.setVisibility(View.VISIBLE);
                                btnPoints.setEnabled(false);

                                long duration = getDateDiff(currentDate, afterAddingThirtyMins, TimeUnit.MILLISECONDS);

                                new CountDownTimer(duration, 1000) {

                                    @Override
                                    public void onTick(long millisUntilFinished) {
                                        String result = String.format("%02d : %02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished), TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished)));

                                        countDown.setText(result);
                                    }

                                    @Override
                                    public void onFinish() {
                                        countDown.setVisibility(View.GONE);
                                        textCountDown.setVisibility(View.GONE);
                                        btnPoints.setEnabled(true);
                                    }

                                }.start();
                            }
                        }
                    }
                });

        return view;
    }

    @Override
    public void onClick(View v) {
        FragmentTransaction tr = getFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.btnPoints:
                db.collection("questions")
                        .get()
                        .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    QuerySnapshot querySnapshot = task.getResult();
                                    int size = querySnapshot.getDocuments().size();
                                    type = rand.nextInt(size - 1) + 1;

                                    QuestionFragment questionFragment = new QuestionFragment();
                                    questionFragment.setAppAndType(application, type);
                                    FragmentTransaction tr = getFragmentManager().beginTransaction();
                                    tr.replace(R.id.container, questionFragment);
                                    tr.addToBackStack(null);
                                    tr.commit();
                                }
                            }

                        });

                break;
            case R.id.btnTrial:
                QuizFragment quizFragment = new QuizFragment();
                quizFragment.setApplication(application);
                tr.replace(R.id.container, quizFragment);
                tr.addToBackStack(null);
                tr.commit();
                break;
        }
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }
}