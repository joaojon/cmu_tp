package pt.ipp.estg.cmu_tp;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import pt.ipp.estg.cmu_tp.ExternAPI.APIClient;
import pt.ipp.estg.cmu_tp.ExternAPI.FootbalAPITeamResponse;
import pt.ipp.estg.cmu_tp.ExternAPI.FootballAPI;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ScannerFragment extends Fragment implements ZXingScannerView.ResultHandler {
    public Application application;
    ZXingScannerView scannerView;
    protected FootbalAPITeamResponse footbalAPITeamResponseList;

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) getContext(), Manifest.permission.CAMERA)) {
                scannerView = new ZXingScannerView(getActivity());
            } else {
                ActivityCompat.requestPermissions((Activity) getContext(), new String[]{Manifest.permission.CAMERA}, 100);
            }
        }

        scannerView = new ZXingScannerView(getActivity());

        return scannerView;
    }


    @Override
    public void onPause() {
        super.onPause();
        scannerView.stopCamera();
    }

    @Override
    public void onResume() {
        super.onResume();
        scannerView.setResultHandler(this);
        scannerView.startCamera();

    }


    @Override
    public void handleResult(final Result rawResult) {
        FootballAPI apiClient = APIClient.getClient().create(FootballAPI.class);
        Call<FootbalAPITeamResponse> call = apiClient.getTeam("39", "2020");

        footbalAPITeamResponseList = new FootbalAPITeamResponse();

        if (isInteger(rawResult.getText())) {

            call.enqueue(new Callback<FootbalAPITeamResponse>() {

                @Override
                public void onResponse(Call<FootbalAPITeamResponse> call, Response<FootbalAPITeamResponse> response) {
                    footbalAPITeamResponseList = response.body();

                    boolean teamFound = false;
                    int listPos = -1;

                    for (int i = 0; i < footbalAPITeamResponseList.getResponse().size(); i++) {
                        if (footbalAPITeamResponseList.getResponse().get(i).getTeam().getId() == Integer.parseInt(rawResult.getText())) {
                            teamFound = true;
                            listPos = i;
                            break;
                        }
                    }

                    Log.d("TAG", "Response = " + response.body().getResults());

                    if (teamFound) {

                        QuestionFragment questionFragment = new QuestionFragment();
                        questionFragment.setAppAndType(application, 1);
                        questionFragment.setCorrectOption(listPos);
                        FragmentTransaction tr = getFragmentManager().beginTransaction();
                        tr.replace(R.id.container, questionFragment);
                        tr.addToBackStack(null);
                        tr.commit();
                    }
                }

                @Override
                public void onFailure(Call<FootbalAPITeamResponse> call, Throwable t) {
                    returnScannerFunction();

                }
            });

        } else {
            returnScannerFunction();
        }
    }

    private void returnScannerFunction() {
        Handler handler = new Handler();
        String warning_message = "The QR Code scanned is invalid!";

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                scannerView.resumeCameraPreview(ScannerFragment.this);
            }
        }, 1000);

        Toast.makeText(getActivity(), "Message: " + warning_message, Toast.LENGTH_SHORT).show();
    }

    private static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c < '0' || c > '9') {
                return false;
            }
        }
        return true;
    }

}
