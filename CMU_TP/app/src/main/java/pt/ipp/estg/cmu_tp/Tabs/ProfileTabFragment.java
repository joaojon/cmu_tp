package pt.ipp.estg.cmu_tp.Tabs;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Map;

import pt.ipp.estg.cmu_tp.ExternAPI.APIClient;
import pt.ipp.estg.cmu_tp.ExternAPI.FootbalAPITeamResponse;
import pt.ipp.estg.cmu_tp.ExternAPI.FootballAPI;
import pt.ipp.estg.cmu_tp.ExternAPI.Response;
import pt.ipp.estg.cmu_tp.ExternAPI.Team;
import pt.ipp.estg.cmu_tp.R;
import retrofit2.Call;
import retrofit2.Callback;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class ProfileTabFragment extends Fragment {

    // Fields
    private TextView name, email, ranking, contact, teamName;
    private ImageView teamLogo;
    private LinearLayout teamSection;

    private FootbalAPITeamResponse apiTeamResponse;

    private Team userTeam;
    private Map<String, Object> user;

    public ProfileTabFragment() {
        // Required empty public constructor
    }

    /**
     * Method to get Team of user
     * Only run on update user if have teamID
     */
    private void getTeam() {

        apiTeamResponse = new FootbalAPITeamResponse();

        FootballAPI apiClient = APIClient.getClient().create(FootballAPI.class);
        Call<FootbalAPITeamResponse> call = apiClient.getTeamByID(user.get("teamId").toString(), "39", "2020");

        call.enqueue(new Callback<FootbalAPITeamResponse>() {
            @Override
            public void onResponse(Call<FootbalAPITeamResponse> call, retrofit2.Response<FootbalAPITeamResponse> response) {
                apiTeamResponse = response.body();
                List<Response> responses = apiTeamResponse.getResponse();


                if (responses.get(0).getTeam() != null) {
                    userTeam = responses.get(0).getTeam();
                    updateUserTeam();
                }

//                updateUserTeam(responses.get(0).getTeam().getName(), responses.get(0).getTeam().getLogo());
            }

            @Override
            public void onFailure(Call<FootbalAPITeamResponse> call, Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
            }
        });
    }

    public void updateUserTeam() {
        if (userTeam != null) {
            teamSection.setVisibility(View.VISIBLE);

            if (teamName != null) {
                teamName.setText(userTeam.getName());
            }

            if (teamLogo != null) {
                Picasso.with(getContext())
                        .load(userTeam.getLogo())
                        .into(teamLogo);
            }

        }
    }

    public void setUser(Map<String, Object> user) {
        this.user = user;

        if (name != null) {
            updateUser();
        }
    }

    private void updateUser() {
        name.setText(user.get("name").toString());
        email.setText(user.get("email").toString());
        ranking.setText(user.get("points").toString());
        contact.setText(user.get("phoneNumber").toString());

        if (user.get("teamId") != null) {
            getTeam();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_profile_tab, container, false);

        // Set fields
        name = view.findViewById(R.id.nameValue);
        email = view.findViewById(R.id.emailValue);
        ranking = view.findViewById(R.id.rankingValue);
        contact = view.findViewById(R.id.phoneValue);

        teamName = view.findViewById(R.id.teamValue);
        teamLogo = view.findViewById(R.id.teamLogo);
        teamSection = view.findViewById(R.id.teamSection);

        if (user != null) {
            updateUser();
        }

        return view;
    }
}