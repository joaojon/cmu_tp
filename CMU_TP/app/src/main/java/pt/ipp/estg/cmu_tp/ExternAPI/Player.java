package pt.ipp.estg.cmu_tp.ExternAPI;

public class Player {
    public int id;
    public String name;
    public String firstname;
    public String lastname;
    public int age;
    public String nationality;
    public String height;
    public String weight;
    public boolean injured;
    public String photo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }
}

