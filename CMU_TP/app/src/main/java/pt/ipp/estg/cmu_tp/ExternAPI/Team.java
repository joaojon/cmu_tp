package pt.ipp.estg.cmu_tp.ExternAPI;

import com.google.gson.annotations.SerializedName;

public class Team {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("country")
    private String country;
    @SerializedName("founded")
    private int founded;
    @SerializedName("national")
    private boolean national;
    @SerializedName("logo")
    private String logo;

    public Team(int id, String name, String country, int founded, boolean national, String logo) {
        this.id = id;
        this.name = name;
        this.country = country;
        this.founded = founded;
        this.national = national;
        this.logo = logo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }
}
