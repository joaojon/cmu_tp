package pt.ipp.estg.cmu_tp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import pt.ipp.estg.cmu_tp.ExternAPI.APIClient;
import pt.ipp.estg.cmu_tp.ExternAPI.FootbalAPITeamResponse;
import pt.ipp.estg.cmu_tp.ExternAPI.FootballAPI;
import pt.ipp.estg.cmu_tp.ExternAPI.VenueAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class VenuesFragment extends Fragment {
    private RecyclerView mRecyclerView;
    protected FootbalAPITeamResponse footbalAPITeamResponseList;

    public VenuesFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_venues, container, false);

        footbalAPITeamResponseList = new FootbalAPITeamResponse();

        mRecyclerView = view.findViewById(R.id.recycler_view);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(layoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(mRecyclerView.getContext(),
                layoutManager.getOrientation());
        mRecyclerView.addItemDecoration(dividerItemDecoration);

        FootballAPI apiClient = APIClient.getClient().create(FootballAPI.class);
        Call<FootbalAPITeamResponse> call = apiClient.getTeam("39", "2020");

        Log.d("TAG", "Response = " + call.request().toString());

        call.enqueue(new Callback<FootbalAPITeamResponse>() {
            @Override
            public void onResponse(Call<FootbalAPITeamResponse> call, Response<FootbalAPITeamResponse> response) {
                footbalAPITeamResponseList = response.body();
                Log.d("TAG", "Response = " + response.body().getResults());


                final VenueAdapter venueAdapter = new VenueAdapter(getContext(), footbalAPITeamResponseList.getResponse());
                mRecyclerView.setAdapter(venueAdapter);
            }

            @Override
            public void onFailure(Call<FootbalAPITeamResponse> call, Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
            }
        });

        return view;
    }
}