package pt.ipp.estg.cmu_tp.Repo;

import android.app.Application;
import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import pt.ipp.estg.cmu_tp.DAOs.QuestionHistoryDao;
import pt.ipp.estg.cmu_tp.Database.AppDatabase;
import pt.ipp.estg.cmu_tp.Entities.QuestionHistory;

public class DatabaseRepository {
    private final QuestionHistoryDao questionHistoryDao;

    public DatabaseRepository(Application application) {
        AppDatabase dataRoombase = AppDatabase.getDatabase(application);
        questionHistoryDao = dataRoombase.questionHistoryDao();
    }

    public void insertQuestionHistory(QuestionHistory history) {
        new insertAsyncTaskQuestionHistory(questionHistoryDao).execute(history);
    }

    public AsyncTask<String, String, List<QuestionHistory>> getAllHistoryByEmail(String email) {
        return new getByEmailAsyncTaskQuestionHistory(questionHistoryDao).execute(email);
    }

    public AsyncTask<String, String, ArrayList<Integer>> countHistoryByType(String email) {
        return new countByTypeAsyncTaskQuestionHistory(questionHistoryDao).execute(email);
    }

    public AsyncTask<String, String, ArrayList<Integer>> countPercentage(String email) {
        return new countCorrectAndIncorrectQuestionHistory(questionHistoryDao).execute(email);
    }

    public static class countCorrectAndIncorrectQuestionHistory extends AsyncTask<String, String, ArrayList<Integer>> {
        private final QuestionHistoryDao mTaskDao;

        public countCorrectAndIncorrectQuestionHistory(QuestionHistoryDao mTaskDao) {
            this.mTaskDao = mTaskDao;
        }

        @Override
        protected ArrayList<Integer> doInBackground(String... emails) {
            ArrayList<Integer> list = new ArrayList<>();

            list.add(mTaskDao.countCorrectAnswers(emails[0])); // 0 => Correct
            list.add(mTaskDao.countIncorrectAnswers(emails[0])); // 1 => Incorrect

            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<Integer> questionHistories) {
            super.onPostExecute(questionHistories);
        }
    }

    public static class countByTypeAsyncTaskQuestionHistory extends AsyncTask<String, String, ArrayList<Integer>> {

        private final QuestionHistoryDao mTaskDao;

        countByTypeAsyncTaskQuestionHistory(QuestionHistoryDao dao) {
            mTaskDao = dao;
        }

        @Override
        protected ArrayList<Integer> doInBackground(String... emails) {
            ArrayList<Integer> list = new ArrayList<>();

            list.add(mTaskDao.countByVenueQuestion(emails[0])); // 0 => Venue
            list.add(mTaskDao.countByPlayerQuestion(emails[0])); // 1 => Player
            list.add(mTaskDao.countByTeamQuestion(emails[0])); // 2 => Team

            return list;
        }

        @Override
        protected void onPostExecute(ArrayList<Integer> questionHistories) {
            super.onPostExecute(questionHistories);
        }
    }

    private static class getByEmailAsyncTaskQuestionHistory extends AsyncTask<String, String, List<QuestionHistory>> {

        private final QuestionHistoryDao mAsyncTaskDao;

        getByEmailAsyncTaskQuestionHistory(QuestionHistoryDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected List<QuestionHistory> doInBackground(String... emails) {
            String email = emails[0];

            List<QuestionHistory> list = mAsyncTaskDao.getAllByEmail(email);

            return list.size() != 0 ? list : null;
        }

        @Override
        protected void onPostExecute(List<QuestionHistory> questionHistories) {
            super.onPostExecute(questionHistories);
        }
    }

    private static class insertAsyncTaskQuestionHistory extends AsyncTask<QuestionHistory, Void, Void> {
        private final QuestionHistoryDao mTaskDao;

        insertAsyncTaskQuestionHistory(QuestionHistoryDao dao) {
            mTaskDao = dao;
        }

        @Override
        protected Void doInBackground(QuestionHistory... questionHistories) {
            mTaskDao.insert(questionHistories[0]);
            return null;
        }
    }
}
