package pt.ipp.estg.cmu_tp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Map;

import pt.ipp.estg.cmu_tp.Adapters.TabAdapter;
import pt.ipp.estg.cmu_tp.Tabs.ProfileTabFragment;
import pt.ipp.estg.cmu_tp.Tabs.RankingTabFragment;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class ProfileFragment extends Fragment {
    // Fields
    private TextView rankingValue, name;
    private LinearLayout layoutContainer;

    private Snackbar snackbar;

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Map<String, Object> userLogged;

    private ProgressBar loading;

    // Tabs
    private TabAdapter tabAdapter;

    private TabLayout tabLayout;
    private ViewPager viewPager;

    private final RankingTabFragment rankingTabFragment = new RankingTabFragment();
    private final ProfileTabFragment profileTabFragment = new ProfileTabFragment();

    private FirebaseUser mUser;

    public ProfileFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Firebase
        mUser = FirebaseAuth.getInstance().getCurrentUser();
    }

    /**
     * Method to remove progressbar from screen
     */
    private void removeLoading() {
        loading.setVisibility(View.GONE);

        if (layoutContainer != null)
            layoutContainer.setVisibility(View.VISIBLE);
    }

    /**
     * Method to set name and ranking on user card
     */
    private void updateUser() {
        rankingValue.setText(userLogged.get("points").toString());
        name.setText(userLogged.get("name").toString());
    }

    /**
     * Method to create tabs and add do adapter
     */
    private void createTabs() {
        // Set user on Profile tab fragment
        profileTabFragment.setUser(userLogged);

        // Set tabs
        tabAdapter.addFragment(profileTabFragment, "Profile");
        tabAdapter.addFragment(rankingTabFragment, "Ranking");

        viewPager.setAdapter(tabAdapter);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_profile, container, false);

        // Linear Layout with content
        layoutContainer = view.findViewById(R.id.container);

        // Progress Bar
        loading = view.findViewById(R.id.progress_bar);

        // Ranking Text
        rankingValue = view.findViewById(R.id.score);
        // Name Text
        name = view.findViewById(R.id.name);

        viewPager = view.findViewById(R.id.view_pager);
        tabLayout = view.findViewById(R.id.tab_layout);

        // Create Adapter
        tabAdapter = new TabAdapter(getChildFragmentManager(), 0);
        tabLayout.setupWithViewPager(viewPager);

        //--> Run Query <--//
        executeQuery();

        return view;
    }

    /**
     * Method where is get user by email run query on Firestore
     */
    private void executeQuery() {
        this.db.collection("users")
                .whereEqualTo("email", this.mUser.getEmail())
                .limit(1)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        // Execute method to remove progress bar and show content
                        removeLoading();

                        if (task.isSuccessful()) {
                            QuerySnapshot doc = task.getResult();
                            userLogged = doc.getDocuments().get(0).getData();

                            // Set name and points text
                            updateUser();

                            // Create tabs
                            createTabs();
                        } else {
                            snackbar = Snackbar.make(getView(), "There was an error fetching the user.", Snackbar.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }
                });
    }
}