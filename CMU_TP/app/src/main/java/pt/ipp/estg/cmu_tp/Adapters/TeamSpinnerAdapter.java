package pt.ipp.estg.cmu_tp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.squareup.picasso.Picasso;

import java.util.List;

import pt.ipp.estg.cmu_tp.ExternAPI.Response;
import pt.ipp.estg.cmu_tp.R;

public class TeamSpinnerAdapter extends ArrayAdapter<List<Response>> {

    private final List<Response> footbalAPITeamResponseList;
    Context mContext;

    public TeamSpinnerAdapter(@NonNull Context context, List<Response> responseList) {
        super(context, R.layout.team_row_item);
        this.footbalAPITeamResponseList = responseList;
        this.mContext = context;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return footbalAPITeamResponseList.size();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ViewHolder mViewHolder = new ViewHolder();

        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.team_row_item, parent, false);
            mViewHolder.mFlag = convertView.findViewById(R.id.teamImg);
            mViewHolder.mName = convertView.findViewById(R.id.teamName);
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) convertView.getTag();
        }

        mViewHolder.mName.setText(footbalAPITeamResponseList.get(position).getTeam().getName());

        Picasso.with(mContext)
                .load(footbalAPITeamResponseList.get(position).getTeam().getLogo())
                .into(mViewHolder.mFlag);

        return convertView;
    }

    private static class ViewHolder {
        ImageView mFlag;
        TextView mName;
    }
}
