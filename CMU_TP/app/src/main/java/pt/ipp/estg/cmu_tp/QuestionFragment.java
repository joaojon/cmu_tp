package pt.ipp.estg.cmu_tp;

import android.app.Application;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Random;

import pt.ipp.estg.cmu_tp.Entities.QuestionHistory;
import pt.ipp.estg.cmu_tp.ExternAPI.APIClient;
import pt.ipp.estg.cmu_tp.ExternAPI.FootbalAPITeamResponse;
import pt.ipp.estg.cmu_tp.ExternAPI.FootballAPI;
import pt.ipp.estg.cmu_tp.ExternAPI.FootballAPIPlayersResponse;
import pt.ipp.estg.cmu_tp.Repo.DatabaseRepository;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class QuestionFragment extends Fragment implements View.OnClickListener {
    private int type;
    private TextView title, question, subtitle, points;
    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    protected FootbalAPITeamResponse footbalAPITeamResponseList;
    private final Random rand = new Random();
    private ImageView venue;
    private RadioGroup radio;
    private Button submitBtn;
    protected ArrayList<Integer> options;
    protected int correctOption = -1;
    protected long questionPoints;
    private static String doc = "";
    private boolean trial = false;

    private Application application;
    private DatabaseRepository dbRoom;

    public QuestionFragment() {
    }

    public void setTrial(boolean trial) {
        this.trial = trial;
    }

    public void setAppAndType(Application app, int type) {
        application = app;
        dbRoom = new DatabaseRepository(application);

        this.type = type;
    }

    public void setCorrectOption(int correctOption) {
        this.correctOption = correctOption;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_question, container, false);

        footbalAPITeamResponseList = new FootbalAPITeamResponse();
        options = new ArrayList<>();


        title = view.findViewById(R.id.quizType);
        question = view.findViewById(R.id.question);
        venue = view.findViewById(R.id.image);
        subtitle = view.findViewById(R.id.subtitle);
        radio = view.findViewById(R.id.radio);
        submitBtn = view.findViewById(R.id.btnSubmit);
        points = view.findViewById(R.id.points);

        db.collection("questions")
                .whereEqualTo("type", type)
                .limit(1)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            QuerySnapshot doc = task.getResult();
                            Map<String, Object> questionEntity;

                            if (doc.getDocuments().size() == 1) {
                                questionEntity = doc.getDocuments().get(0).getData();
                            } else {
                                questionEntity = doc.getDocuments().get(doc.getDocuments().size() - 1).getData();
                            }
                            question.setText(questionEntity.get("text").toString());
                            if (trial) {
                                points.setVisibility(View.GONE);
                            } else {
                                points.setText(questionEntity.get("points").toString() + " points");
                            }
                            questionPoints = (long) questionEntity.get("points");
                        }
                    }
                });
        if (type == 1) {
            title.setText("Venue Question");
            venueQuestionLogic();
        } else if (type == 2) {
            title.setText("Player Question");
            playerQuestionLogic();
        } else if (type == 3) {
            title.setText("Team Question");
            teamQuestionLogic();
        }


        submitBtn.setOnClickListener(this);

        return view;
    }

    private void playerQuestionLogic() {
        FootballAPI apiClient = APIClient.getClient().create(FootballAPI.class);
        Call<FootbalAPITeamResponse> call = apiClient.getTeam("39", "2020");

        call.enqueue(new Callback<FootbalAPITeamResponse>() {
            @Override
            public void onResponse(Call<FootbalAPITeamResponse> call, Response<FootbalAPITeamResponse> response) {
                footbalAPITeamResponseList = response.body();
                Log.d("View", "Response = " + response.body().getResults());

                correctOption = rand.nextInt(footbalAPITeamResponseList.getResponse().size() - 1);

                while (options.size() < 3) {
                    int option = rand.nextInt(footbalAPITeamResponseList.getResponse().size() - 1);

                    if (!options.contains(option) && option != correctOption) {
                        options.add(option);
                    }
                }

                findRandomPlayerOfTeam(footbalAPITeamResponseList.getResponse().get(correctOption).getTeam().getId());

                options.add(correctOption);
                Collections.shuffle(options);

                RadioButton button;
                int id = 1;
                for (int i : options) {
                    button = new RadioButton(getContext());
                    button.setText(footbalAPITeamResponseList.getResponse().get(i).getTeam().getName());
                    button.setId(id++);
                    radio.addView(button);
                }
            }

            @Override
            public void onFailure(Call<FootbalAPITeamResponse> call, Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
            }
        });
    }

    private void findRandomPlayerOfTeam(int teamID) {
        FootballAPI apiClient = APIClient.getClient().create(FootballAPI.class);
        Call<FootballAPIPlayersResponse> call = apiClient.getPlayersByTeamID(String.valueOf(teamID), "2020");

        call.enqueue(new Callback<FootballAPIPlayersResponse>() {
            @Override
            public void onResponse(Call<FootballAPIPlayersResponse> call, Response<FootballAPIPlayersResponse> response) {
                final FootballAPIPlayersResponse footbalAPIPlayersResponseList = response.body();

                int playerPos = rand.nextInt(footbalAPIPlayersResponseList.getResponse().size() - 1);

                Picasso.with(getContext())
                        .load(footbalAPIPlayersResponseList.getResponse().get(playerPos).getPlayer().getPhoto())
                        .placeholder(R.drawable.loading_animation)
                        .into(venue);
                venue.getLayoutParams().height = 400;
                venue.getLayoutParams().width = 400;
                subtitle.setVisibility(View.VISIBLE);
                subtitle.setText("(" + footbalAPIPlayersResponseList.getResponse().get(correctOption).getPlayer().getName() + ")");
            }

            @Override
            public void onFailure(Call<FootballAPIPlayersResponse> call, Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
            }
        });
    }

    private void teamQuestionLogic() {
        FootballAPI apiClient = APIClient.getClient().create(FootballAPI.class);
        Call<FootbalAPITeamResponse> call = apiClient.getTeam("39", "2020");

        call.enqueue(new Callback<FootbalAPITeamResponse>() {
            @Override
            public void onResponse(Call<FootbalAPITeamResponse> call, Response<FootbalAPITeamResponse> response) {
                footbalAPITeamResponseList = response.body();
                Log.d("View", "Response = " + response.body().getResults());

                correctOption = rand.nextInt(footbalAPITeamResponseList.getResponse().size() - 1);

                while (options.size() < 3) {
                    int option = rand.nextInt(footbalAPITeamResponseList.getResponse().size() - 1);

                    if (!options.contains(option) && option != correctOption) {
                        options.add(option);
                    }
                }

                Picasso.with(getContext())
                        .load(footbalAPITeamResponseList.getResponse().get(correctOption).getTeam().getLogo())
                        .placeholder(R.drawable.loading_animation)
                        .into(venue, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                                venue.getLayoutParams().height = 400;
                                venue.getLayoutParams().width = 400;
                            }

                            @Override
                            public void onError() {
                                venue.setVisibility(View.GONE);
                                subtitle.setVisibility(View.VISIBLE);
                                subtitle.setText("(" + footbalAPITeamResponseList.getResponse().get(correctOption).getTeam().getLogo() + ")");
                            }
                        });

                options.add(correctOption);
                Collections.shuffle(options);

                RadioButton button;
                int id = 1;
                for (int i : options) {
                    button = new RadioButton(getContext());
                    button.setText(footbalAPITeamResponseList.getResponse().get(i).getVenue().getName());
                    button.setId(id++);
                    radio.addView(button);
                }
            }

            @Override
            public void onFailure(Call<FootbalAPITeamResponse> call, Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
            }
        });
    }

    private void venueQuestionLogic() {
        FootballAPI apiClient = APIClient.getClient().create(FootballAPI.class);
        Call<FootbalAPITeamResponse> call = apiClient.getTeam("39", "2020");

        call.enqueue(new Callback<FootbalAPITeamResponse>() {
            @Override
            public void onResponse(Call<FootbalAPITeamResponse> call, Response<FootbalAPITeamResponse> response) {
                footbalAPITeamResponseList = response.body();
                Log.d("View", "Response = " + response.body().getResults());


                Log.d("Team", String.valueOf(correctOption));
                if (correctOption == -1) {
                    correctOption = rand.nextInt(footbalAPITeamResponseList.getResponse().size() - 1);
                }
                while (options.size() < 3) {
                    int option = rand.nextInt(footbalAPITeamResponseList.getResponse().size() - 1);

                    if (!options.contains(option) && option != correctOption) {
                        options.add(option);
                    }
                }

                Picasso.with(getContext())
                        .load(footbalAPITeamResponseList.getResponse().get(correctOption).getVenue().getImage())
                        .placeholder(R.drawable.loading_animation)
                        .into(venue, new com.squareup.picasso.Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                                venue.setVisibility(View.GONE);
                                subtitle.setVisibility(View.VISIBLE);
                                subtitle.setText("(" + footbalAPITeamResponseList.getResponse().get(correctOption).getVenue().getName() + ")");
                            }
                        });

                options.add(correctOption);
                Collections.shuffle(options);

                RadioButton button;
                int id = 1;
                for (int i : options) {
                    button = new RadioButton(getContext());
                    button.setText(footbalAPITeamResponseList.getResponse().get(i).getTeam().getName());
                    button.setId(id++);
                    radio.addView(button);
                }
            }

            @Override
            public void onFailure(Call<FootbalAPITeamResponse> call, Throwable t) {
                Log.d("TAG", "Response = " + t.toString());
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnSubmit:
                int selectedId = radio.getCheckedRadioButtonId();

                FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();

                if (radio.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(getContext(), "Please select an option", Toast.LENGTH_SHORT).show();
                } else {
                    if (options.get(selectedId - 1) == correctOption) {

                        if (!trial) {
                            db.collection("users")
                                    .whereEqualTo("email", mUser.getEmail())
                                    .limit(1)
                                    .get()
                                    .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                        @Override
                                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                            if (task.isSuccessful()) {
                                                QuerySnapshot querySnapshot = task.getResult();
                                                doc = querySnapshot.getDocuments().get(0).getId();

                                                DocumentReference dbRef = db.collection("users").document(doc);
                                                dbRef.update("points", FieldValue.increment(questionPoints));
                                            }
                                        }

                                    });
                            Toast.makeText(getContext(), "Correct! " + questionPoints + " points were added.", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getContext(), "Correct! ", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getContext(), "Wrong! The right answer was " + footbalAPITeamResponseList.getResponse().get(correctOption).getTeam().getName(), Toast.LENGTH_SHORT).show();
                    }

                    if (!trial) {
                        db.collection("users")
                                .whereEqualTo("email", mUser.getEmail())
                                .limit(1)
                                .get()
                                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                    @Override
                                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                        if (task.isSuccessful()) {
                                            QuerySnapshot querySnapshot = task.getResult();
                                            doc = querySnapshot.getDocuments().get(0).getId();

                                            DocumentReference dbRef = db.collection("users").document(doc);
                                            dbRef.update("lastDateOfAnswered", new Timestamp(new Date()));
                                        }
                                    }

                                });

                        QuestionHistory history = new QuestionHistory(
                                mUser.getEmail(),
                                question.getText().toString(),
                                type,
                                (options.get(selectedId - 1) == correctOption) ? (int) questionPoints : 0, footbalAPITeamResponseList.getResponse().get(correctOption).getTeam().getName(),
                                footbalAPITeamResponseList.getResponse().get(options.get(selectedId - 1)).getTeam().getName());
                        dbRoom.insertQuestionHistory(history);
                    }
                    ProfileFragment profileFragment = new ProfileFragment();
                    FragmentTransaction tr = getFragmentManager().beginTransaction();
                    tr.replace(R.id.container, profileFragment);
                    tr.addToBackStack(null);
                    tr.commit();
                    break;
                }
        }
    }
}