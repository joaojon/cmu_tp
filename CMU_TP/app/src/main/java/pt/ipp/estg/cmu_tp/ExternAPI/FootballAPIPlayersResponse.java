package pt.ipp.estg.cmu_tp.ExternAPI;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FootballAPIPlayersResponse {
    @SerializedName("get")
    public String get;
    @SerializedName("parameters")
    public Parameters parameters;
    @SerializedName("errors")
    public List<Object> errors;
    @SerializedName("results")
    public int results;
    @SerializedName("paging")
    public Paging paging;
    @SerializedName("response")
    public List<ResponsePlayers> response;

    public FootballAPIPlayersResponse() {
    }

    public List<ResponsePlayers> getResponse() {
        return response;
    }
}
