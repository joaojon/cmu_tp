package pt.ipp.estg.cmu_tp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;

import java.util.List;

import pt.ipp.estg.cmu_tp.R;

public class UsersAdapter extends
        RecyclerView.Adapter<UsersAdapter.ViewHolder> {

    private final List<DocumentSnapshot> mUsers;
    private final Context context;

    public UsersAdapter(List<DocumentSnapshot> users, Context ctx) {
        mUsers = users;
        this.context = ctx;
    }

    @NonNull
    @Override
    public UsersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(this.context);

        View contactView = inflater.inflate(R.layout.user_row_item, parent, false);

        return new ViewHolder(contactView);
    }

    /**
     * Called by <strong>RecyclerView</strong> to display the user in specified position (using <code>.get(position)</code>).
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(UsersAdapter.ViewHolder holder, int position) {
        DocumentSnapshot user = mUsers.get(position);

        TextView userName = holder.userName;
        TextView userRanking = holder.userRanking;
        TextView userPosition = holder.position;

        TextView me = holder.me;

        if (this.isLoggedUser(user.get("email").toString())) {
            me.setVisibility(View.VISIBLE);
            me.setText("(me!)");
        }

        // Position = position + 1
        userPosition.setText(String.valueOf(position + 1));

        userName.setText(user.get("name").toString());
        userRanking.setText(user.get("points").toString());
    }

    /**
     * Method to return the number of users
     *
     * @return int <strong>Number of Users</strong>
     */
    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    public boolean isLoggedUser(String email) {
        FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();

        assert mUser != null;
        return mUser.getEmail().equals(email);
    }

    /**
     * ViewHolder where is set all properties of layout file (<code>user_row_item.xml</code>)
     * for example <code>TextViews</code>
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView userName;
        public TextView userRanking;
        public TextView position;
        public ImageView userAvatar;

        public TextView me;

        public ViewHolder(View itemView) {
            super(itemView);

            userName = itemView.findViewById(R.id.cardName);
            userRanking = itemView.findViewById(R.id.cardRanking);
            position = itemView.findViewById(R.id.position);
            userAvatar = itemView.findViewById(R.id.cardAvatar);

            me = itemView.findViewById(R.id.me);
        }
    }
}