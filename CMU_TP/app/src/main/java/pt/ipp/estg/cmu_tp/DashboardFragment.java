package pt.ipp.estg.cmu_tp;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.List;
import java.util.Map;

import pt.ipp.estg.cmu_tp.Adapters.TabAdapter;
import pt.ipp.estg.cmu_tp.Entities.QuestionHistory;
import pt.ipp.estg.cmu_tp.Repo.DatabaseRepository;
import pt.ipp.estg.cmu_tp.Tabs.ChartsTabFragment;
import pt.ipp.estg.cmu_tp.Tabs.HistoryTabFragment;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class DashboardFragment extends Fragment {

    private Application app;

    private FirebaseUser mUser;

    private final FirebaseFirestore db = FirebaseFirestore.getInstance();
    private Map<String, Object> userLogged;

    private Snackbar snackbar;

    private ProgressBar loading;

    // Tabs Fragments
    private final ChartsTabFragment chartsTab = new ChartsTabFragment();
    private final HistoryTabFragment historyTab = new HistoryTabFragment();

    // Tabs
    private TabAdapter tabAdapter;
    private LinearLayout layoutContainer;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private TextView userName;

    public DashboardFragment() {
        // Required empty public constructor
    }

    public void setApp(Application application) {
        app = application;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mUser = FirebaseAuth.getInstance().getCurrentUser();
    }


    /**
     * Method to remove progressbar from screen
     */
    private void removeLoading() {
        loading.setVisibility(View.GONE);

        if (layoutContainer != null) {
            layoutContainer.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Method to set name and ranking on user card
     */
    private void updateUser() {
        userName.setText(userLogged.get("name").toString());
    }

    /**
     * Method to create tabs and add do adapter
     */
    private void createTabs() {
        // Set user on Profile tab fragment
        chartsTab.setAppAndEmail(app, mUser.getEmail());
        historyTab.setAppAndEmail(app, mUser.getEmail());

        // Set tabs
        tabAdapter.addFragment(chartsTab, "Statistics");
        tabAdapter.addFragment(historyTab, "History");

        viewPager.setAdapter(tabAdapter);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        userName = view.findViewById(R.id.userName);

        // Linear Layout with content
        layoutContainer = view.findViewById(R.id.containerDashboard);

        // Progress Bar
        loading = view.findViewById(R.id.progress_bar);

        viewPager = view.findViewById(R.id.view_pager);
        tabLayout = view.findViewById(R.id.tab_layout);

        // Create Adapter
        tabAdapter = new TabAdapter(getChildFragmentManager(), 0);
        tabLayout.setupWithViewPager(viewPager);

        //--> Run Query <--//
        executeQuery();

        return view;
    }

    /**
     * Method where is get user by email run query on Firestore
     */
    private void executeQuery() {
        db.collection("users")
                .whereEqualTo("email", mUser.getEmail())
                .limit(1)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        // Execute method to remove progress bar and show content
                        removeLoading();

                        if (task.isSuccessful()) {
                            QuerySnapshot doc = task.getResult();
                            userLogged = doc.getDocuments().get(0).getData();

                            // Set name and points text
                            updateUser();

                            // Create tabs
                            createTabs();
                        } else {
                            snackbar = Snackbar.make(getView(), "There was an error fetching the user.", Snackbar.LENGTH_SHORT);
                            snackbar.show();
                        }
                    }
                });
    }
}