package pt.ipp.estg.cmu_tp.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import pt.ipp.estg.cmu_tp.Entities.QuestionHistory;
import pt.ipp.estg.cmu_tp.R;

public class QuestionHistoriesAdapter extends
        RecyclerView.Adapter<QuestionHistoriesAdapter.ViewHolder> {

    private final List<QuestionHistory> mHistory;
    private final Context context;

    public QuestionHistoriesAdapter(List<QuestionHistory> history, Context ctx) {
        mHistory = history;
        context = ctx;
    }

    @NonNull
    @Override
    public QuestionHistoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.history_row_item, parent, false);

        return new QuestionHistoriesAdapter.ViewHolder(contactView);
    }

    /**
     * Called by <strong>RecyclerView</strong> to display the user in specified position (using <code>.get(position)</code>).
     *
     * @param holder   The ViewHolder which should be updated to represent the contents of the
     *                 item at the given position in the data set.
     * @param position The position of the item within the adapter's data set.
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(QuestionHistoriesAdapter.ViewHolder holder, int position) {
        QuestionHistory history = mHistory.get(position);

        TextView question = holder.question;
        TextView answer = holder.answer;
        TextView timestamp = holder.timestamp;
        TextView points = holder.points;
        ImageView answerImg = holder.answerImg;
        Drawable icon = (sameAnswer(history.userAnswer, history.correctAnswer)) ? ContextCompat.getDrawable(context, R.drawable.ic_baseline_check_circle_24) : ContextCompat.getDrawable(context, R.drawable.ic_baseline_cancel_24);

        question.setText(history.question);
        answer.setText(history.userAnswer);
        points.setText(String.valueOf(history.points));

        timestamp.setText(formatDate(history.timestamp));

        if (sameAnswer(history.userAnswer, history.correctAnswer)) {
            answer.setText(history.correctAnswer);
        } else {
            answer.setText(history.userAnswer + " (correct is " + history.correctAnswer + ")");
        }

        answerImg.setImageDrawable(icon);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private String formatDate(String timestamp) {

        try {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
            LocalDateTime dateTime = LocalDateTime.parse(timestamp);
            String formattedDateTime = dateTime.format(formatter);

            return formattedDateTime;
        } catch (DateTimeException exc) {
            Log.d("Error Timestamp Format", "Invalid format!");
            return timestamp;
        }
    }

    /**
     * Method to return the number of users
     *
     * @return int <strong>Number of Users</strong>
     */
    @Override
    public int getItemCount() {
        return mHistory.size();
    }

    private boolean sameAnswer(String user, String correct) {
        return user.equals(correct);
    }

    /**
     * ViewHolder where is set all properties of layout file (<code>user_row_item.xml</code>)
     * for example <code>TextViews</code>
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView question, answer, timestamp, points;
        public LinearLayout answerContainer;

        public ImageView answerImg;

        public ViewHolder(View itemView) {
            super(itemView);

            answerImg = itemView.findViewById(R.id.answerImg);
            question = itemView.findViewById(R.id.question);
            answer = itemView.findViewById(R.id.answer);
            answerContainer = itemView.findViewById(R.id.answerContainer);
            timestamp = itemView.findViewById(R.id.timestamp);
            points = itemView.findViewById(R.id.points);
        }
    }
}
