package pt.ipp.estg.cmu_tp;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginFragment extends Fragment implements View.OnClickListener {

    private TextInputEditText email, password;
    private FirebaseAuth mAuth;
    private Button loginBtn, registerBtn;

    private Application application;

    public LoginFragment() {
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        email = view.findViewById(R.id.email);
        password = view.findViewById(R.id.password);
        loginBtn = view.findViewById(R.id.btnLogin);
        registerBtn = view.findViewById(R.id.btnRegister);

        loginBtn.setOnClickListener(this);
        registerBtn.setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();

        return view;
    }

    @Override
    public void onClick(View v) {
        //do what you want to do when button is clicked
        switch (v.getId()) {
            case R.id.btnLogin:
                authenticate(email.getText().toString(), password.getText().toString());
                break;
            case R.id.btnRegister:
                RegisterFragment registerFragment = new RegisterFragment();
                FragmentTransaction tr = getParentFragmentManager().beginTransaction();
                tr.replace(R.id.login_fragment, registerFragment);
                tr.addToBackStack(null);
                tr.commit();
                break;
        }
    }

    private void authenticate(String email, String password) {
        if (!checkForm()) {
            Toast.makeText(getContext(), "Some field is missing.", Toast.LENGTH_SHORT).show();
            return;
        }

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(getContext(), "Login successfully", Toast.LENGTH_SHORT).show();

                            HomeFragment homeFragment = new HomeFragment();
                            homeFragment.setApplication(application);
                            FragmentTransaction tr = getFragmentManager().beginTransaction();
                            tr.replace(R.id.login_fragment, homeFragment);
                            tr.addToBackStack(null);
                            tr.commit();
                        } else {
                            Toast.makeText(getContext(), "Login failed, try again please.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getContext(), "Login failed, try again please.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private boolean checkForm() {
        return !email.getText().toString().isEmpty() && !password.getText().toString().isEmpty();
    }
}
