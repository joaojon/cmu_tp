package pt.ipp.estg.cmu_tp.Database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import pt.ipp.estg.cmu_tp.DAOs.QuestionHistoryDao;
import pt.ipp.estg.cmu_tp.Entities.QuestionHistory;

@Database(entities = {QuestionHistory.class}, version = 2, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {
    public abstract QuestionHistoryDao questionHistoryDao();

    private static final String DB_NAME = "footballQuizDB";
    private static AppDatabase INSTANCE;

    private static final int NUMBER_OF_THREADS = 4;

    public static AppDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AppDatabase.class, DB_NAME)
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}

