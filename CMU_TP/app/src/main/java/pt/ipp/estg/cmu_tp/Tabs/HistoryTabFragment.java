package pt.ipp.estg.cmu_tp.Tabs;

import android.app.Application;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.concurrent.ExecutionException;

import pt.ipp.estg.cmu_tp.Adapters.QuestionHistoriesAdapter;
import pt.ipp.estg.cmu_tp.Entities.QuestionHistory;
import pt.ipp.estg.cmu_tp.R;
import pt.ipp.estg.cmu_tp.Repo.DatabaseRepository;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class HistoryTabFragment extends Fragment {

    private Application app;
    private DatabaseRepository dbRoom;
    private String mUser;

    private TextView noData;

    private QuestionHistoriesAdapter adapter;
    private RecyclerView rvHistories;
    private List<QuestionHistory> listOfHistory;

    public HistoryTabFragment() {
        // Required empty public constructor
    }

    public void setAppAndEmail(Application app, String email) {
        this.app = app;
        mUser = email;

        dbRoom = new DatabaseRepository(app);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_history_tab, container, false);

        rvHistories = view.findViewById(R.id.rvHistory);
        noData = view.findViewById(R.id.noData);

        try {
            getHistory();
        } catch (ExecutionException | InterruptedException e) {
            showNoDataMsg();
        }

        return view;
    }

    private void showNoDataMsg() {
        rvHistories.setVisibility(View.GONE);
        noData.setVisibility(View.VISIBLE);
    }

    private void showHistory() {
        adapter = new QuestionHistoriesAdapter(listOfHistory, getContext());
        rvHistories.setLayoutManager(new LinearLayoutManager(getContext()));
        rvHistories.setAdapter(adapter);
    }

    private void getHistory() throws ExecutionException, InterruptedException {
        listOfHistory = dbRoom.getAllHistoryByEmail(mUser).get();

        if (listOfHistory != null) {
            showHistory();
        } else {
            showNoDataMsg();
        }
    }
}