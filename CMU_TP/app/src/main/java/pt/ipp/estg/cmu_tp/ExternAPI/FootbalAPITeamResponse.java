package pt.ipp.estg.cmu_tp.ExternAPI;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FootbalAPITeamResponse {
    @SerializedName("get")
    public String get;
    @SerializedName("parameters")
    public Parameters parameters;
    @SerializedName("errors")
    public List<Object> errors;
    @SerializedName("results")
    public int results;
    @SerializedName("paging")
    public Paging paging;
    @SerializedName("response")
    public List<Response> response;

    public FootbalAPITeamResponse() {
    }

    public String getGet() {
        return get;
    }

    public void setGet(String get) {
        this.get = get;
    }

    public int getResults() {
        return results;
    }

    public List<Response> getResponse() {
        return response;
    }
}
