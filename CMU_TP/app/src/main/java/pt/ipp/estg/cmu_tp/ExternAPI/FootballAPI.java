package pt.ipp.estg.cmu_tp.ExternAPI;

import pt.ipp.estg.cmu_tp.BuildConfig;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface FootballAPI {
    @Headers("x-rapidapi-key: " + BuildConfig.API_KEY)
    @GET("teams")
    Call<FootbalAPITeamResponse> getTeam(@Query("league") String league, @Query("season") String season);

    @Headers("x-rapidapi-key: " + BuildConfig.API_KEY)
    @GET("teams")
    Call<FootbalAPITeamResponse> getTeamByID(@Query("id") String id, @Query("league") String league, @Query("season") String season);

    @Headers("x-rapidapi-key: " + BuildConfig.API_KEY)
    @GET("players")
    Call<FootballAPIPlayersResponse> getPlayersByTeamID(@Query("team") String team, @Query("season") String season);
}
