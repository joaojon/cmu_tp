package pt.ipp.estg.cmu_tp.ExternAPI;

import com.google.gson.annotations.SerializedName;

public class Venue {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("address")
    private String address;
    @SerializedName("city")
    private String city;
    @SerializedName("capacity")
    private int capacity;
    @SerializedName("surface")
    private String surface;
    @SerializedName("image")
    private String image;

    public Venue(int id, String name, String address, String city, int capacity, String surface, String image) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.capacity = capacity;
        this.surface = surface;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public String getImage() {
        return image;
    }
}
