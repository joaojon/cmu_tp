package pt.ipp.estg.cmu_tp.ExternAPI;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import pt.ipp.estg.cmu_tp.HomeFragment;
import pt.ipp.estg.cmu_tp.MapFragment;
import pt.ipp.estg.cmu_tp.R;

public class VenueAdapter extends RecyclerView.Adapter<VenueAdapter.VenueViewHolder> {
    private final Context mContext;
    private final List<Response> footbalAPITeamResponseList;

    public VenueAdapter(Context mContext, List<Response> footbalAPITeamResponseList) {
        this.mContext = mContext;
        this.footbalAPITeamResponseList = footbalAPITeamResponseList;
    }

    @Override
    public VenueViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.venue, parent, false);
        return new VenueViewHolder(view);
    }

    @Override
    public void onBindViewHolder(VenueViewHolder viewHolder, int position) {
        viewHolder.teamNameTextView.setText(footbalAPITeamResponseList.get(position).getTeam().getName());
        viewHolder.venueNameTextView.setText(footbalAPITeamResponseList.get(position).getVenue().getName());

        final String address = footbalAPITeamResponseList.get(position).getVenue().getAddress();
        final String venueName = footbalAPITeamResponseList.get(position).getVenue().getName();

        Picasso.with(mContext)
                .load(footbalAPITeamResponseList.get(position).getTeam().getLogo())
                .placeholder(R.drawable.loading_animation)
                .into(viewHolder.teamLogo);
        viewHolder.btn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                MapFragment fragment = new MapFragment();
                Bundle args = new Bundle();
                args.putString("address", address);
                args.putString("venueName", venueName);
                fragment.setArguments(args);

                FragmentTransaction tr = ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction();
                tr.replace(R.id.container, fragment);
                tr.addToBackStack(null);
                tr.commit();

            }
        });
    }

    @Override
    public int getItemCount() {
        if (footbalAPITeamResponseList != null) {
            return footbalAPITeamResponseList.size();
        }
        return 0;
    }

    public class VenueViewHolder extends RecyclerView.ViewHolder {
        public TextView teamNameTextView, venueNameTextView;
        public ImageView teamLogo;
        final public ImageButton btn;


        public VenueViewHolder(View itemView) {
            super(itemView);

            teamNameTextView = itemView.findViewById(R.id.team_name);
            venueNameTextView = itemView.findViewById(R.id.venue_name);
            teamLogo = itemView.findViewById(R.id.team_logo);
            btn = itemView.findViewById(R.id.btn);
        }
    }
}
