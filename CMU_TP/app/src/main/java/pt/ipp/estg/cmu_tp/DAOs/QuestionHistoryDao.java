package pt.ipp.estg.cmu_tp.DAOs;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import pt.ipp.estg.cmu_tp.Entities.QuestionHistory;

@Dao
public interface QuestionHistoryDao {
    @Insert
    void insert(QuestionHistory questionHistory);

    @Query("SELECT * FROM questionhistory WHERE user_email LIKE :email ORDER BY timestamp DESC")
    List<QuestionHistory> getAllByEmail(String email);

    @Query("SELECT COUNT(question_type) FROM questionhistory WHERE user_email LIKE :email AND question_type = 1 GROUP BY question_type")
    int countByVenueQuestion(String email);

    @Query("SELECT COUNT(question_type) FROM questionhistory WHERE user_email LIKE :email AND question_type = 2 GROUP BY question_type")
    int countByPlayerQuestion(String email);

    @Query("SELECT COUNT(question_type) FROM questionhistory WHERE user_email LIKE :email AND question_type = 3 GROUP BY question_type")
    int countByTeamQuestion(String email);

    @Query("SELECT COUNT(question) FROM questionhistory WHERE user_email LIKE :email AND user_answer = correct_answer")
    int countCorrectAnswers(String email);

    @Query("SELECT COUNT(question) FROM questionhistory WHERE user_email LIKE :email AND correct_answer NOT LIKE user_answer")
    int countIncorrectAnswers(String email);
}
