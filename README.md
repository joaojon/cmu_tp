# Projeto de Computação Móvel e Ubíqua (CMU)
<a href="#"><img src="https://gitlab.com/joaojon/cmu_tp/-/raw/master/CMU_TP/app/src/main/res/drawable/football_quiz.png" width="300px" alt="Logo"></a>

### **Intrução:**
No âmbito da uc Computação Movel e Ubíqua, ́e pretendido que através da elaboração deste trabalho prático seja posto em prático todos os conhecimentos adquiridos ao longo desta. Assim sendo, foi necessário inicialmente proceder à escolha do desporto, desporto  este que possibilitaria à aplicação acompanhar uma ́epoca desportiva e os várias eventos realizados. Após a escolha recair sobre o futebol, foi necessário então analisar todos os recursos disponíveis que poderiam ser utilizados nesta aplicação.

### **Relatório:**

<a href="https://gitlab.com/joaojon/cmu_tp/-/blob/master/Relatorio.pdf">Link</a>

### **Ferramentas utilizadas:**
* GitLab

 <a href="#"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/GitLab_Logo.svg/1108px-GitLab_Logo.svg.png" width="100px" alt="Gitlab"></a>

* Android Studio

<a href="#"><img src="https://2.bp.blogspot.com/-tzm1twY_ENM/XlCRuI0ZkRI/AAAAAAAAOso/BmNOUANXWxwc5vwslNw3WpjrDlgs9PuwQCLcBGAsYHQ/s1600/pasted%2Bimage%2B0.png" width="100px" alt="AndroidStudio"></a>

* JAVA

<a href="#"><img src="https://logodownload.org/wp-content/uploads/2017/04/java-logo-2.png" width="100px" alt="Postman"></a>
